#!/usr/bin/env python3

"""
This is the main module of the sorting visualizer

Author:  Jin Meyer
Author:  Morgaux Meyer
License: MIT (C) <AUTHORS> 2020
"""

import sorting
import drawing

def main():
    pass

# call main()
if __name__ == "__main__":
    main()

