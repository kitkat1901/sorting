#!/usr/bin/env python3

"""
This is the sorting module to provide different sorting methods

Author:  Jin Meyer
Author:  Morgaux Meyer
License: MIT (C) <AUTHORS> 2020
"""

# this function returns True if the items are sorted and False if they are not
def isSorted(items):
    return all(items[i] <= items[i + 1] for i in range(len(items) - 1))

def bubbleSort(items):
    raise NotImplementedError

def quickSort(items):
    raise NotImplementedError

